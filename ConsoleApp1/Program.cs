﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace ConsoleApp1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            var serviceProvider = services.BuildServiceProvider();

            // entry to run app
            var app = serviceProvider.GetService<App>() ?? throw new Exception("App not found");
            await app.Run(args);
        }


        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient<App>();
        }
    }
}