﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class App
    {
        private Uri _address;
        private int _interval;

        private readonly HttpClient _httpClient;

        public App(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private string CheckStatus(HttpStatusCode status)
        {
            return status == HttpStatusCode.OK ? "OK(200)" : $"ERR({status})";
        }

        public async Task Run(string[] args)
        {
            _interval = int.Parse(args[0]);

            try
            {
                _address = new Uri(args[1]);
            }
            catch (UriFormatException)
            {
                Console.WriteLine("URL parsing error");
                return;
            }


            for (;;)
            {
                var response = await _httpClient.GetAsync(_address);
                Console.WriteLine($"Checking '{_address}'. Result: {CheckStatus(response.StatusCode)}");
                Thread.Sleep(TimeSpan.FromSeconds(_interval));
            }
        }
    }
}